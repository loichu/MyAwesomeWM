# MyAwesomeWM

Awesome WM configuration

## Required packages
* xorg-xbacklight
* lm-sensors

## Backlight
See [official documentation](https://wiki.archlinux.org/index.php/backlight)

Allow users in group `video` to change brightness.

`/etc/udev/rules.d/backlight.rules`
```
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"
```