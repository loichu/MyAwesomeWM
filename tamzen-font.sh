if [ ! -n "$(pacman -Qs tamzen-font-git)" ]; then
    curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=tamzen-font-git
    makepkg PKGBUILD --install --needed
fi
